import { Component, OnInit } from '@angular/core';
import { Produit } from '../models/produit.model';
import { ProduitService } from '../services/produit.service';

@Component({
  selector: 'app-produits',
  templateUrl: './produits.component.html',
})
export class ProduitsComponent implements OnInit {
  products?: Produit[];

  constructor(private produitService: ProduitService) {}

  ngOnInit(): void {
    this.products = this.produitService.listeProduits();
  }

  supprimerProduit(prod: Produit) {
    let conf = confirm('Etes-vous sûr ?');

    if (conf) {
      this.produitService.supprimerProduit(prod);
    }
  }
}
